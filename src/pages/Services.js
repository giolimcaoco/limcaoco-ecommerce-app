import {Fragment, useEffect, useState, useContext} from 'react';
//import { Container } from 'react-bootstrap';

import AdminView from '../components/AdminView';
import ClientView from '../components/ClientView';
//import ServicesCard from '../components/ServicesCard';

import UserContext from '../UserContext';


export default function Services(){

	const { client } = useContext(UserContext);
	const [services, setServices] = useState ([]);

	
	const fetchData = () => {

			let token = localStorage.getItem('token')

			 fetch(`${process.env.REACT_APP_API_URL}/services/allServices`, {
			 	method: "GET",
			 	headers: {
			 		"Authorization": `Bearer ${token}`,
			 		'Content-Type':'application/json',
			 		'Access-Control-Allow-Origin':'*',
			 		'Access-Control-Allow-Methods':'POST,PATCH,OPTIONS'

			 	}
			 })
			     .then(res => res.json())
			     .then(res => {
			    	setServices(res);
			     });      



	    }


	useEffect(() => {
		fetchData()
	},[])

		return(
			<Fragment>
				
				{(client.id !== null && client.isAdmin ===true)
					? 	<AdminView servicesData={services} fetchData={fetchData}/>             
				    : 	<ClientView servicesData={services}/>  			                
				}				
			</Fragment>
	)
}


				// <Fragment>
				// <Banner data={data}/>
    //             <ClientView servicesData={services}/>
    //             </Fragment>