import { Form, Button, Container } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import { Navigate, Link } from 'react-router-dom'

// UserContext
import UserContext from '../UserContext'

// sweetalert
import Swal from 'sweetalert2';

export default function Login(){
	
	const { client, setClient } = useContext(UserContext);

	const [ email, setEmail ] = useState ('');
	const [ password, setPassword ] = useState ('');
	const [ isActive, setIsActive ] = useState (false);

	const retrieveUserDetails = (token) => {
		fetch (`${process.env.REACT_APP_API_URL}/clients/details`, {
			headers: {
				Authorization: `Bearer ${token}`,
				'Content-Type':'application/json',
				
			}
		})
		.then(res => res.json())
		.then(data => {
			
			setClient ({
				id: data._id,
				isAdmin: data.isAdmin
			})
		})
	}

	useEffect(() => {
		if (email !== '' && password !== '' ) {
			setIsActive (true)
		} else {
			setIsActive (false)
		}
	},[email, password]);

	function loginUser(e){
		e.preventDefault();


		fetch(`${process.env.REACT_APP_API_URL}/clients/login`,{
			method: 'POST',
			headers: {
				'Content-type': 'application/json',
				
			},
			body: JSON.stringify({
				email: email, 
				password: password
			})
		})
		.then(res => res.json())		
		.then(data => {		

		console.log(data)	
	
			if (typeof data.access !== 'undefined') {
				localStorage.setItem('token', data.access);
				retrieveUserDetails(data.access)

				Swal.fire({
					title: "Login successful!",
					icon: "success",
					text: "Hello! Welcome back"
				})
			} else {
				Swal.fire({
					title: "Authentication Failed",
					icon: "error",
					text: "Check your login credentials and try again."
				})
			}
		})	
		
		setEmail('');
		setPassword('');
		
	}	


	return(
			(client.id !== null) ?
				<Navigate to='/services' />
			:
			<Container className="font mt-5">
				<h1 className="text-uppercase text-center mb-5">Login</h1>
				<Form onSubmit={(e) => loginUser(e)}>
				  <Form.Group className="my-3" controlId="userEmail">
				    <Form.Label>Email address</Form.Label>
				    <Form.Control 
				    		type="email" 
				    		placeholder="Enter email" 
				    		value = {email}
				    		onChange= { e=> setEmail(e.target.value) }
				    		required 
				    		/>
				  </Form.Group>

				  <Form.Group className="my-3" controlId="password" required>
				    <Form.Label>Password</Form.Label>
				    <Form.Control 
				    		type="password" 
				    		placeholder="Password"
				    		value = {password}
				    		onChange= { e=> setPassword(e.target.value) } 
				    		/>
				  </Form.Group>

				
				  { isActive ?
						  <Button variant="secondary" type="submit" id="submitBtn" className="my-3">
						    Login
						  </Button>
						:
						  <Button variant="secondary" type="submit" id="submitBtn" className="my-3" disabled>
						    Login
						  </Button>
				  }

				  <p>Not yet registered? <Link to="/register">Sign up here</Link></p>

				</Form>
			</Container>
		)
	}




