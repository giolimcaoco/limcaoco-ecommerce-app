import {Fragment} from 'react';
import Banner from '../components/Banner';
import Highlights from '../components/Highlights';



export default function Home(){

	const data = {
	    title: "Virtual Assistance Services",
	    content: "We see things holistically",
	    destination: "/register",
	    label: "Get your FREE discovery call today!"
	}


	return (
		<Fragment>
			<Banner data={data}/>
			<Highlights />
		</Fragment>
	)
}
