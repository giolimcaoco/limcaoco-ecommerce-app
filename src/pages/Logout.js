import { Navigate } from 'react-router-dom';
import { useContext, useEffect } from 'react';

import UserContext from '../UserContext'

export default function Logout() {
	
	const {unsetClient, setClient} = useContext(UserContext);
	
	// clear the localStorage
	unsetClient(); 

	
	useEffect(()=>{
		// set the user state back to its original state
		setClient({id:null})
	})

	return (
		<Navigate to ='/'/>
	)
}
