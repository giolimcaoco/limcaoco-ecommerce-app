import { Form, Button, Container } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import { Navigate, useNavigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';

// UserContext
import UserContext from '../UserContext'

export default function Register(){

		const { client } = useContext(UserContext);

		const navigate = useNavigate()
		
		// state hooks to store the values of the input fields
		const [ email, setEmail ] = useState ('');
		const [ fullName, setFullName ] = useState ('');
		//const [ lastName, setLastName ] = useState ('');
		const [ mobileNo, setMobileNo ] = useState ('');
		const [ password1, setPassword1 ] = useState ('');
		const [ password2, setPassword2 ] = useState ('');
		// state hooks to determine whether the submit button is enabled or not
		const [ isDisabled, setIsDisabled ] = useState (false);
		
		console.log (email);
		console.log (fullName);
		//console.log (lastName);
		console.log (mobileNo);
		console.log (password1);
		console.log (password2);

		useEffect(() => {
			if ((email !== '' && fullName !== '' && mobileNo.length === 11 && password1 !== '' && password2 !== '' ) && (password1 === password2)) {
				setIsDisabled (true)
			} else {
				setIsDisabled (false)
			}
		},[email, fullName, mobileNo, password1, password2]);
		

		function registerUser(e){
			e.preventDefault();		


			fetch(`${process.env.REACT_APP_API_URL}/clients/checkEmail`, {
				method: "POST",
				headers: {
					"Content-Type": "application/json",					
			 		
				},
				body: JSON.stringify ({
					email: email
				})
			})
			.then (res => res.json())
			.then (res =>  {

				if (res === true){
					Swal.fire ({
						title: 'Email already exists, please login instead',
						icon: 'error',
						text: 'You may also reach out to your assigned VA'
					})
					
				} else {

					fetch (`${process.env.REACT_APP_API_URL}/clients/register`, {
						method: "POST",
						headers: {							
							'Content-Type':'application/json',
							
						},
						body: JSON.stringify({
							email: email,
							fullName: fullName, 
							//lastName: lastName,
							mobileNo: mobileNo,
							password: password1
							
						})
					})
					.then (res => res.json())
					.then (res => {
						if (res === true) {
							Swal.fire({
								title: "Thank you for registering!",
								icon: "success",
								text: "One of our VAs will get in touch to schedule a DISCOVERY call"
							})
							navigate('/login')
						} else {
							Swal.fire({
								title: "Something went wrong",
								icon: "error",
								text: "Please try again and fill out the form."
							})
						}
					})
				}
			})

			setFullName('');
			//setLastName('');
			setMobileNo('');
			setEmail('');
			setPassword1('');
			setPassword2('');

			console.log('Registration successful')

		}

	return (
		(client.id !== null) ?
			<Navigate to='/login' />
		:
		<Container className="font mt-5">
			<h1 className="text-uppercase text-center mb-5">Register for an account</h1>
	    	<Form onSubmit = {(e)=>registerUser(e)}>
	      		<Form.Group controlId="userEmail" className="my-3">
	       			<Form.Label>Email address</Form.Label>
	        			<Form.Control	        				
	        				type="email" 
	        				placeholder="Enter email"			   				
	        				value={email}
	        				onChange={e => setEmail(e.target.value)}
	        				required/>	        			
	        			<Form.Text className="text-muted">We'll never share your email with anyone else.</Form.Text>
	     		</Form.Group>

	     		<Form.Group controlId="fullName" className="my-3">
	     		    <Form.Label>Full Name</Form.Label>
	     			    <Form.Control 
	     		    		type="fullName" 
	     		    		placeholder="First Name and Last Name" 
	     		    		value={fullName}
	     		    		onChange={e => setFullName(e.target.value)}
	     		    		required/>
	     		</Form.Group>
	     		

	     		<Form.Group controlId="mobileNo" className="my-3">
	     		    <Form.Label>Mobile Number</Form.Label>
	     			    <Form.Control 
	     		    		type="mobileNo" 
	     		    		placeholder="+639xxxxxxxxx" 
	     		    		value={mobileNo}
	     		    		onChange={e => setMobileNo(e.target.value)}
	     		    		required/>
	     		</Form.Group>

	     		<Form.Group controlId="password1" className="my-3">
			        <Form.Label>Password</Form.Label>
			    	    <Form.Control 
			        		type="password" 
			        		placeholder="Create Password" 
			        		value={password1}
			        		onChange={e => setPassword1(e.target.value)}
			        		required/>
			    </Form.Group>

			    <Form.Group controlId="password2" className="my-3">
			        <Form.Label>Verify Password</Form.Label>
			    		<Form.Control 
			        		type="password" 
			        		placeholder="Re-enter Password" 
			        		value={password2}
			        		onChange={e => setPassword2(e.target.value)}
			        		required/>
			    </Form.Group>
	    		{ isDisabled ? 
				
	      			<Button variant="secondary" type="submit" id="submitBtn" className="my-3">Submit</Button>
	  	  		
	      			:
				
	      			<Button variant="secondary" type="submit" id="submitBtn" className="my-3" disabled>Submit</Button>
	  	  	
	  	  		}

	  	  		<p>Already registered? <Link to="/login">Login here</Link></p>
		    </Form>
	    </Container>
	  );

} 
