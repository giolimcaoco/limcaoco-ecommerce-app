import {Fragment, useEffect, useState, useContext} from 'react';
//import { Container } from 'react-bootstrap';
import Banner from '../components/Banner';
import ClientsList from '../components/ClientsList';
import ClientView from '../components/ClientView';
//import ServicesCard from '../components/ServicesCard';

import UserContext from '../UserContext';


export default function Services(){

	const { client } = useContext(UserContext);
	const [clients, setClients] = useState ([]);

	

	const fetchData = () => {

			let token = localStorage.getItem('token')

			 fetch(`${process.env.REACT_APP_API_URL}/clients/allClients`, {
			 	method: "GET",
			 	headers: {
			 		"Authorization": `Bearer ${token}`,
			 				"Content-Type": "application/json",					
			 		 		'Access-Control-Allow-Origin':'*',
			 		 		'Access-Control-Allow-Methods':'POST,PATCH,OPTIONS'
			 	}
			 })
			     .then(res => res.json())
			     .then(res => {
			    	setClients(res);
			     });      



	    }


	useEffect(() => {
		fetchData()
	},[])

		return(
			<Fragment>
				
				{(client.isAdmin === true)
				                ? <ClientsList clientsData={clients} fetchData={fetchData}/>
				                : <ClientsList clientsData={clients} fetchData={fetchData}/>
				            }				
			</Fragment>
	)
}
