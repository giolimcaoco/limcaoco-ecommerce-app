import { Fragment, useContext } from 'react';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import {Container} from 'react-bootstrap';
import { Link, NavLink } from 'react-router-dom';
import UserContext from './UserContext'
import './App.css';

export default function AppNavbar(){
	const { client } = useContext(UserContext)	;

	// return(
	// 			<Navbar bg="secondary" expand="lg">
	// 				<Navbar.Brand as={ Link } to='/' className="my-nav m-2 px-2">VA SERVICES</Navbar.Brand>
	// 				<Navbar.Toggle aria-controls="basic-navbar-nav" />
	// 				<Navbar.Collapse id="basic-navbar-nav">
	// 					<Nav className="my-nav ml-auto">
	// 						<Nav.Link as={ NavLink } to='/' exact>Home</Nav.Link>
	// 						<Nav.Link as={ NavLink } to='/services' exact> Services</Nav.Link>
							
	// 						{(client.id !== null) ?
	// 							<Nav.Link as={ NavLink } to='/logout' exact> Logout</Nav.Link>
	// 						:
	// 							<Fragment>
	// 								<Nav.Link as={ NavLink } to='/login' exact> Login</Nav.Link>
	// 								<Nav.Link as={ NavLink } to='/register' exact> Register</Nav.Link>
	// 							</Fragment>
	// 						}
	// 					</Nav>
	// 				</Navbar.Collapse>
	// 			</Navbar>
	// 		)


	// let navigate = useNavigate();

	// const logout = () => {
	// 	unsetClient();
	// 	navigate.push ('/')
	// }


	// let clientLogIn = //(client.id !== null) ?
	// 	(client.isAdmin === true) ?
	// 	(client.id === null) ?

	// 		<Fragment>
	// 			<Nav.Link as= { NavLink } to='/' className="my-nav" exact="true">Home</Nav.Link>
				
			
	// 		</Fragment>
	// 		:
	// 		<Fragment className="adminNav">
	// 			<Nav.Link as= { NavLink } to='/services' className="my-nav" exact> Admin Dashboard Services List</Nav.Link>
	// 			<Nav.Link as= { NavLink } to='/clientele' className="my-nav" exact> Clients List</Nav.Link>
	// 			<Nav.Link as={NavLink} to="/logout" className="my-nav" exact>Logout</Nav.Link>
	// 		</Fragment>
	// 	:
		
	// 		(

	// 			<Fragment>					
	// 				<Nav.Link as= { NavLink } to='/' className="my-nav" exact="true">Home</Nav.Link>
	// 				<Nav.Link as= { NavLink } to='/services' className="my-nav" exact="true"> Services1</Nav.Link>
	// 				<Nav.Link as= { NavLink } to='/login' className="my-nav" exact="true"> Client Portal</Nav.Link>
	// 				<Nav.Link as= { NavLink } to='/register' className="my-nav" exact="true"> Hire a VA today!</Nav.Link>
	// 			</Fragment>

	// 		)




	// 		return (
			
	// 			<Navbar bg="secondary" expand="lg">
	// 				<Navbar.Brand as= { Link } to='/' className="my-nav m-2 px-2">VA Services</Navbar.Brand>
	// 					<Navbar.Toggle aria-controls="basic-navbar-nav" />
	// 						<Navbar.Collapse id="basic-navbar-nav">
	// 							<Nav className="my-nav ml-auto">
	// 								{clientLogIn}
	// 							</Nav>
	// 						</Navbar.Collapse>
	// 					</Navbar>

	// 			)
//}





	// return(
	// 		<Navbar bg="secondary" expand="lg">
	// 			<Navbar.Brand as= { Link } to='/' className="my-nav m-2 px-2">VA Services</Navbar.Brand>
	// 			<Navbar.Toggle aria-controls="basic-navbar-nav" />
	// 			<Navbar.Collapse id="basic-navbar-nav">
	// 				<Nav className="my-nav ml-auto">
	// 					<Nav.Link as= { NavLink } to='/' className="my-nav" exact>Home</Nav.Link>
	// 					<Nav.Link as= { NavLink } to='/services' className="my-nav" exact> Services</Nav.Link>

	// 					{ 	(client.id === null) ?			
							
	// 						<Fragment>
	// 							<Nav.Link as= { NavLink } to='/login' className="my-nav" exact> Client Portal</Nav.Link>
	// 							<Nav.Link as= { NavLink } to='/register' className="my-nav" exact> Hire a VA today!</Nav.Link>
	// 						</Fragment>


							
	// 					:

	// 					 	(client.isAdmin === true) ?			
							
	// 						<Fragment>
								
	// 							<Nav.Link as= { NavLink } to='/clientele' className="my-nav" exact> Clients</Nav.Link>
	// 							<Nav.Link as={NavLink} to="/logout" className="my-nav" exact>Logout</Nav.Link>
	// 						</Fragment>

	// 						:

	// 						 	(client.id !== null) ?			
								
	// 							<Fragment>	
	// 								<Nav.Link as= { NavLink } to='/services' className="my-nav" exact> Profile</Nav.Link>								
	// 								<Nav.Link as= { NavLink } to='/services' className="my-nav" exact> Invoices</Nav.Link>
	// 								<Nav.Link as={NavLink} to="/logout" className="my-nav" exact>Logout</Nav.Link>
	// 							</Fragment>

							
							


	// 							:
	// 							<Nav.Link as={NavLink} to="/logout" className="my-nav" exact>Logout</Nav.Link>


	// 					}
							

						






	// 				</Nav>
	// 			</Navbar.Collapse>
	// 		</Navbar>
	// 	)
//}

	return(
		<Navbar bg="secondary" expand="lg" >
		  <Container fluid className="my-nav mx-2">
		    <Navbar.Brand as={Link} to="/" className="my-nav" >VA Services</Navbar.Brand>
		    <Navbar.Toggle aria-controls="navbarScroll" />
		    <Navbar.Collapse id="navbarScroll">
		      <Nav
		        className="me-auto my-2 my-lg-0"
		        style={{ maxHeight: '100px' }}
		        navbarScroll
		      >
		        <Nav.Link as={NavLink} to="/" className="my-nav">Home</Nav.Link>
		        <Nav.Link as={NavLink} to="/services" className="my-nav" exact>Services</Nav.Link>
		        
		        {
	        		client.id !== null && client.isAdmin !== true ?
		        	<Fragment>
		        	
		        	<Nav.Link as={NavLink} to="/Profile" className="my-nav" exact >Profile</Nav.Link>
		        	<Nav.Link as={NavLink} to="/billing" className="my-nav" exact >Billing History</Nav.Link>
		        	<Nav.Link as={NavLink} to="/:userId/cart"className="my-nav"  exact>Cart</Nav.Link>
		        	</Fragment>
		        	:
		        	<Fragment>
		        	<Nav.Link as={NavLink} to="/wishlist" className="my-nav" exact disabled hidden >Profile</Nav.Link>
		        	<Nav.Link as={NavLink} to="/:userId/cart" className="my-nav"  exact disabled hidden>Cart</Nav.Link>
		        	</Fragment>

		        }
		        {
	        		client.id !== null && client.isAdmin === true ?
		        	<Fragment>
		        		
	        			<Nav.Link as={NavLink} to="/clientele" className="my-nav" exact>Clients</Nav.Link>
	        			<Nav.Link as={NavLink} to="/billing" className="my-nav" exact>Invoices</Nav.Link>
		        	</Fragment>
	        		:
	        			<Nav.Link as={NavLink} to="/clientele" className="my-nav" disabled hidden exact>Clients</Nav.Link>
		        }


		      </Nav>
		      <Nav>
		      { client.id !== null ?
		        	<Fragment>
		        	<Nav.Link className="my-nav"  as={NavLink} to="/logout" exact>Logout</Nav.Link>
		        	</Fragment>
		        	:
		        	<Fragment>
				        	<Nav.Link as={NavLink} to="/login" className="my-nav" exact>Client Portal Login</Nav.Link>
				        	<Nav.Link as={NavLink} to="/register" className="my-nav" exact>Hire a VA today!</Nav.Link>
		        	</Fragment>
		        }
		      </Nav>
		    </Navbar.Collapse>
		  </Container>
		</Navbar>		
	)
};



