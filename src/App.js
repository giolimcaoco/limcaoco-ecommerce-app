import './App.css';

// UserContext
import {UserProvider} from './UserContext'


import { useState, useEffect} from 'react';
import {Container} from 'react-bootstrap';
import { BrowserRouter as Router, Route, Routes} from 'react-router-dom';

// Web Components
import AppNavBar from './AppNavBar';

// Web Pages
import Home from './pages/Home.js';
import Services from './pages/Services'
import ServiceView from './components/ServiceView'
import Clientele from './pages/Clientele'
import ClientsList from './components/ClientsList'
import Register from './pages/Register'
import Login from './pages/Login'
import Logout from './pages/Logout'
import Error from './pages/Error'



function App() {

  
    const [title, setTitle] = useState("VA Services")

    useEffect(() => {
        
        document.title = title;
      }, [title]);
  
  
  const [client, setClient] = useState ({ 
     
     email: null,
     isAdmin: null
  });

  const unsetClient = () => {
    localStorage.clear();
  }

  useEffect(()=>{
      
    }, [client])

  return (
    <UserProvider value={{client, setClient, unsetClient}}>
      <Router>
        <AppNavBar />
          <Container fluid>
            <Routes>
              <Route path ="/" element= {<Home />} />
              <Route path ="/services" element= {<Services />} />
              <Route path ="/services/:serviceId" element= {<ServiceView />} />
              <Route path ="/services" element= {<Services />} />
              <Route path ="/clientele" element= {<Clientele />} />              
              <Route path ="/register" element= {<Register />} />
              <Route path ="/login" element= {<Login />} />
              <Route path ="/logout" element= {<Logout />} />
              <Route path='*' element={<Error />} />
            </Routes>
          </Container>
        </Router>         
    </UserProvider>
  );
}




export default App;


