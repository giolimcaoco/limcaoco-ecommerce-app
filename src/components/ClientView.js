import { Fragment, useState, useEffect } from 'react'
import ServicesCard from "./ServicesCard";
import Banner from '../components/Banner';

export default function ClientView({servicesData}) {

    // console.log(coursesData);

    const [services, setServices] = useState([]);

    const data = {
        title: "Hire an experienced Virtual Assistant",
        content: "Let us know how we can help",
        destination: "/register",
        label: "Get your FREE discovery call today!"
    }

        useEffect(() => {
        
        // Map through the courses received from the parent component (courses page) to render the course cards
        const servicesArr = servicesData.map(service => {
            // Returns active courses as "CourseCard" components
        	if(service.isActive === true){
				return (
					<ServicesCard serviceProp={service} key={service._id}/>
				)
        	}else{
        		return null;
        	}
        });

        // Set the "courses" state with the course card components returned by the map method
        // Allows the course card components to be rendered in this "UserView" component via the return statement below
        setServices(servicesArr);

    }, [servicesData]);

    return(
        <Fragment>
            <Banner data={data}/>
            {services}
        </Fragment>
    );
}
