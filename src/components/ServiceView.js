import {useState, useEffect, useContext, Fragment} from 'react';
import {Container, Card, Button, Row, Col} from 'react-bootstrap';
import {useParams, useNavigate, Link} from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function CourseView() {

	const {client} = useContext(UserContext);

	const navigate = useNavigate()

	const {serviceId} = useParams ();

	const [serviceName, setServiceName] = useState ('')
	const [description, setDescription] = useState ('')
	const [price, setPrice] = useState (0)

	const getService = (serviceId) => {
		fetch(`${process.env.REACT_APP_API_URL}/billing/newBill`, {
			method: "POST",
			headers: {
				'Content-type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`,
									
				 		'Access-Control-Allow-Origin':'*',
				 		'Access-Control-Allow-Methods':'POST,PATCH,OPTIONS'
			},
			body: JSON.stringify({
				serviceId: serviceId
			})
		})
		.then(res => res.text())
		.then(data => {
			console.log(data)

			if (data !== false) {
				Swal.fire ({
					title: "Service has been added",
					icon: "success",
					text: "Your VA will connect with you soon"
				})

				navigate('/services')

			} else {
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again"
				})
			}
		})
	}

	useEffect(()=>{
		console.log(serviceId)

		fetch(`${ process.env.REACT_APP_API_URL}/services/${serviceId}`)
				.then(res=>res.json())
				.then(data=>{
					console.log(data)

					setServiceName(data.serviceName);
					setDescription(data.description);
					setPrice(data.price);
				})

	},[serviceId])

	return(
			<Container className='mt-5'>
				<Row>
					<Col lg={ {span: 6, offset: 3} }>
						<Card className="mb-2">
							  <Card.Body className='text-center'>
								    <Card.Header className="text-uppercase" as="h1">{serviceName}</Card.Header>
								    <Card.Text className="mt-4">Description:</Card.Text>
								    <Card.Text>{description}</Card.Text>
								    <Card.Text>Price:</Card.Text>
								    <Card.Text>Php {price}</Card.Text>
								   
								    {
								    	(client.id !== null) ?
								    		<Fragment>
								    		<Link className="btn btn-secondary btn-block mx-2" to="/services">Back to services</Link>
								    		<Button variant="secondary mx-2" onClick={()=>getService(serviceId)} block>Get Service</Button>
								    		</Fragment>
								    		:
								    		<Fragment>
								    		<Link className="btn btn-secondary btn-block mx-2" to="/services">Back to services</Link>
								    		<Link className="btn btn-danger btn-block mx-2" to="/register">Schedule a Discovery call today!</Link>
								    		</Fragment>
								    }

							  </Card.Body>
						</Card>
					</Col>
				</Row>
			</Container>
		)

}

