import { Fragment, useState, useEffect } from 'react';
import { Table, Button, Modal, Form } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function AdminView(props){

	//console.log(props);
	// Destructure our courses data from the props being passed by the parent component (courses page)
	// Includes the "fetchData" function that retrieves the courses from our database
	const { servicesData, fetchData } = props;
	

	// States for form inputs
	const [serviceId, setServiceId] = useState("");
	const [services, setServices] = useState([]);
	const [serviceName, setServiceName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);

	// States to open/close modals
	const [showEdit, setShowEdit] = useState(false);
	const [showAdd, setShowAdd] = useState(false);

	// Functions to toggle the opening and closing of the "Add Course" modal
	const openAdd = () => setShowAdd(true);
	const closeAdd = () => setShowAdd(false);

	/* 
	Function to open the "Edit Course" modal:
		- Fetches the selected course data using the course ID
		- Populates the values of the input fields in the modal form
		- Opens the "Edit Course" modal
	*/
	const openEdit = (serviceId) => {

		// Fetches the selected course data using the course ID
		fetch(`${ process.env.REACT_APP_API_URL}/services/${ serviceId }`)
		.then((res) => res.json())
		.then((data) => {

			console.log(data);

			// Changes the states for binded to the input fields
			// Populates the values of the input files in the modal form
			setServiceId(data._id);
			setServiceName(data.serviceName);
			setDescription(data.description);
			setPrice(data.price);
		})
		

		// Opens the "Edit Course" modal
		setShowEdit(true);
	};

	/* 
	Function to close our "Edit Course" modal:
		- Reset from states back to their initial values
		- Empties the input fields in the form whenever the modal is opened for adding a course
	*/
	const closeEdit = () => {

		setShowEdit(false);
		setServiceName("");
		setDescription("");
		setPrice(0);

	};



	const addService = (e) => {

		// Prevent the form from redirecting to a different page on submit 
		// Helps retain the data if adding a course is unsuccessful
		e.preventDefault()

		fetch(`${ process.env.REACT_APP_API_URL }/services`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				'Accept': 'application/json',	
				Authorization: `Bearer ${ localStorage.getItem('token') }`,
				'Access-Control-Allow-Origin':'*',
				'Access-Control-Allow-Methods':'POST,PATCH,OPTIONS'
			},
			body: JSON.stringify({
				serviceName: serviceName,
				description: description,
				price: price
			})
		})
		.then(res => res.text())
		.then(data => {

			console.log(data)

			// If the new course is successfully added
			if (data !== true) {

				// Invoke the "fetchData" function passed from our parent component (courses page)
				// Rerenders the page because of the "useEffect"
				fetchData();

				// Show a success message via sweet alert
				Swal.fire({
					title: "Success",
					icon: "success",
					text: "Service has been successfully added."					
				})

				// Reset all states to their initial values
				// Provides better user experience by clearing all the input fieles when the user adds another course
				setServiceName("")
				setDescription("")
				setPrice(0)

				// Close the modal
				closeAdd();

			} else {

				fetchData();

				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again."
				})
			}
		})
	}

	const editService = (e, serviceId) => {
		
		e.preventDefault();

		fetch(`${ process.env.REACT_APP_API_URL }/services/${ serviceId }`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${ localStorage.getItem('token') }`,
				'Access-Control-Allow-Origin':'*',
				'Access-Control-Allow-Methods':'POST,PATCH,OPTIONS'
			},
			body: JSON.stringify({
				serviceName: serviceName,
				description: description,
				price: price
			})
		})
		.then(res => res.json())
		.then(data => {

			if (data === true) {

				fetchData();

				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again."
				});

				setServiceName("")
				setDescription("")
				setPrice(0)

				closeEdit();

			} else {

				fetchData();

				Swal.fire({
					
					title: "Success",
					icon: "success",
					text: "Service has been successfully updated."
				});

			}

		})
	}

	// Map through the courses received from the parent component (course page)
	// Re-renders the table whenever the "coursesData" is updated by adding, editing and deleting a course
	useEffect(() => {

		const archiveToggle = (serviceId, isActive) => {

			console.log(!isActive);

			fetch(`${ process.env.REACT_APP_API_URL }/services/${ serviceId }/archive`, {
				method: 'PUT',
				headers: {
					"Content-Type": "application/json",
					'Accept': 'application/json',
					Authorization: `Bearer ${ localStorage.getItem('token') }`,
					'Access-Control-Allow-Origin':'*',
					'Access-Control-Allow-Methods':'POST,PATCH,OPTIONS'
				},
				body: JSON.stringify({
					isActive: !isActive
				})
			})
			.then(res => res.text())
			.then(data => {

				if (data !== false) {

					fetchData();

					Swal.fire({
						title: "Success",
						icon: "success",
						text: "Course successfully archived."
					});

				} else {

					fetchData();

					Swal.fire({
						title: "Something went wrong",
						icon: "error",
						text: "Please try again."
					});

				}
			})

	

		}

		const unArchiveToggle = (serviceId, isActive) => {
				fetch(`${ process.env.REACT_APP_API_URL }/services/${ serviceId }/unArchived`, {
					method: "PUT",
					headers: {
						"Content-Type": "application/json",
						'Accept': 'application/json',
						"Authorization": `Bearer ${ localStorage.getItem('token') }`,
						'Access-Control-Allow-Origin':'*',
						'Access-Control-Allow-Methods':'POST,PATCH,OPTIONS'
					},
					body: JSON.stringify({
						isActive: !isActive
					})
				})
				.then(res => res.json())
				.then(data => {
					fetchData();
					if(data === true){
						Swal.fire({
							title: "Success",
							icon: "success",
							text: "Service successfully reactivated."
							}
						)
					} else {
						fetchData();
						Swal.fire({
							title: "Something went wrong",
							icon: "error",
							text: "Please try again."
						})
					}
				})
			}



		const servicesArr = servicesData.map(service => {

			return(
				
				<tr key={service._id}>
					<td >{service.serviceName}</td>
					<td>{service.description}</td>
					<td>{service.price}</td>
					<td>
						{/* 
							- If the course's "isActive" field is "true" displays "available"
							- Else if the course's "isActive" field is "false" displays "unavailable"
						*/}
						{service.isActive
							? <span>Active</span>
							: <span>Not Active</span>
						}
					</td>
					<td>
						<Button
							variant="muted"
							size="sm"
							onClick={() => openEdit(service._id)}
							className="m-1"
						>
							Update
						</Button>
						{/* 
							- Display a red "Disable" button if course is "active"
							- Else display a green "Enable" button if course is "inactive"
						*/}
						{service.isActive ===true
							?
							<Button 
								variant="muted" 
								size="sm" 
								onClick={() => archiveToggle(service._id, service.isActive)}
								className="m-1"
							>
								Disable
							</Button>
							:
							<Button 
								variant="muted"
								size="sm"
								onClick={() => unArchiveToggle(service._id, service.isActive)}
								className="m-1"
							>
								Enable
							</Button>
						}
					</td>
				</tr>

			)

		});

		// Set the "courses" state with the table rows returned by the map function
		// Renders table row elements inside the table via this "AdminView" return statement below
		setServices(servicesArr);

	}, [servicesData, fetchData]);

	return(
		<Fragment>

			<div className="text-center my-4">
				<h2>Admin Dashboard</h2>
				<div className="d-flex justify-content-center">
					<Button variant="secondary" onClick={openAdd}>Add New Service</Button>			
				</div>			
			</div>

			<Table  hover responsive>
				<thead className="bg-secondary text-white">
					<tr>
						<th>Service Name</th>
						<th>Description</th>
						<th>Price</th>
						<th>Availability</th>
						<th>Actions</th>
					</tr>					
				</thead>
				<tbody>
					{services}
				</tbody>
			</Table>

			{/*ADD MODAL*/}
			<Modal show={showAdd} onHide={closeAdd}>
				<Form onSubmit={e => addService(e)}>
					<Modal.Header closeButton>
						<Modal.Title>Add Service</Modal.Title>
					</Modal.Header>
					<Modal.Body>	
						<Form.Group controlId="serviceName">
							<Form.Label>Service</Form.Label>
							<Form.Control type="text" value={serviceName} onChange={e => setServiceName(e.target.value)} required/>
						</Form.Group>
						<Form.Group controlId="serviceDescription">
							<Form.Label>Description</Form.Label>
							<Form.Control type="text" as="textarea" rows="5" value={description}  onChange={e => setDescription(e.target.value)} required/>
						</Form.Group>
						<Form.Group controlId="servicePrice">
							<Form.Label>Price</Form.Label>
							<Form.Control type="number" value={price}  onChange={e => setPrice(e.target.value)} required/>
						</Form.Group>
					</Modal.Body>
					<Modal.Footer>
						<Button variant="secondary" onClick={closeAdd}>Close</Button>
						<Button variant="success" type="submit">Submit</Button>
					</Modal.Footer>
				</Form>
			</Modal>

			{/*EDIT MODAL*/}
			<Modal show={showEdit} onHide={closeEdit}>
				<Form onSubmit={e => editService(e, serviceId)}>
					<Modal.Header closeButton>
						<Modal.Title>Edit Service</Modal.Title>
					</Modal.Header>
					<Modal.Body>	
						<Form.Group controlId="serviceName">
							<Form.Label>Service</Form.Label>
							<Form.Control type="text" value={serviceName} onChange={e => setServiceName(e.target.value)} required/>
						</Form.Group>
						<Form.Group controlId="serviceDescription">
							<Form.Label>Description</Form.Label>
							<Form.Control type="text" as="textarea" rows="5"  value={description}  onChange={e => setDescription(e.target.value)} required/>
						</Form.Group>
						<Form.Group controlId="servicePrice">
							<Form.Label>Price</Form.Label>
							<Form.Control type="number" value={price}  onChange={e => setPrice(e.target.value)} required/>
						</Form.Group>
					</Modal.Body>
					<Modal.Footer>
						<Button variant="secondary" onClick={closeEdit}>Close</Button>
						<Button variant="success" type="submit">Submit</Button>
					</Modal.Footer>
				</Form>
			</Modal>
			
		</Fragment>
	)
}
