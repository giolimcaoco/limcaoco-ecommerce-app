import {Row, Col, Card} from 'react-bootstrap';

export default function Highlights(){
	return(
		
			<Row className = "mx-5">
				<Col xs={12} md={4}>
					<Card className = "cardHighlight p-3">
						<Card.Body>
							<Card.Title>
								<h2 >Outsource mundane tasks</h2>
							</Card.Title>
							<Card.Text className="pt-2">
								Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus rutrum est orci, at fringilla purus vulputate vitae. Proin lacinia ultricies blandit. Duis tincidunt vulputate tortor vel consectetur. Nam aliquet odio sit amet lectus maximus, sed sollicitudin risus porttitor. Proin eu elit vel nibh dictum sagittis.
							</Card.Text>
						</Card.Body>
					</Card>
				</Col>
				<Col xs={12} md={4}>
					<Card className = "cardHighlight p-3">
						<Card.Body>
							<Card.Title>
								<h2>How our VAs work</h2>
							</Card.Title>
							<Card.Text className="pt-2">
								Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus rutrum est orci, at fringilla purus vulputate vitae. Proin lacinia ultricies blandit. Duis tincidunt vulputate tortor vel consectetur. Nam aliquet odio sit amet lectus maximus, sed sollicitudin risus porttitor. Proin eu elit vel nibh dictum sagittis.
							</Card.Text>
						</Card.Body>
					</Card>
				</Col>
				<Col xs={12} md={4}>
					<Card className = "cardHighlight p-3">
						<Card.Body>
							<Card.Title>
								<h2>Why find your VAs with us?</h2>
							</Card.Title>
							<Card.Text className="pt-2">
								Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus rutrum est orci, at fringilla purus vulputate vitae. Proin lacinia ultricies blandit. Duis tincidunt vulputate tortor vel consectetur. Nam aliquet odio sit amet lectus maximus, sed sollicitudin risus porttitor. Proin eu elit vel nibh dictum sagittis.
							</Card.Text>
						</Card.Body>
					</Card>
				</Col>
			</Row>
			
		)
}
