import {Row, Col} from 'react-bootstrap';
//import {Fragment} from 'react';
//import {Navigate} from 'react-router-dom';
import { Link } from 'react-router-dom';
import '../App.css';



export default function Banner({data}){
	const {title, content, destination, label} = data;
	return(
		<Row>
			<Col className="p-5 m-3">
				<h1>{title}</h1>
				<p>{content}</p>
				<Link className="btn btn-secondary" to={destination}>{label}</Link>
			</Col>
		</Row>
	)
}


