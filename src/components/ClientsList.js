import { Fragment, useState, useEffect } from 'react';
import { Table, Button, Modal, Form } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function AdminView(props){

	//console.log(props);
	// Destructure our courses data from the props being passed by the parent component (courses page)
	// Includes the "fetchData" function that retrieves the courses from our database
	
	const { clientsData, fetchData } = props;

	// States for form inputs

	const [clientId, setClientId] = useState("");
	const [clients, setClients] = useState([]);
	const [fullName, setFullName] = useState("");
	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");
	const [mobileNo, setMobileNo] = useState("");
	const [isAdmin, setIsAdmin] = useState("")
	const [isActive, setIsActive] = useState("")


	// States to open/close modals
	const [showEdit, setShowEdit] = useState(false);
	const [showAdd, setShowAdd] = useState(false);

	// Functions to toggle the opening and closing of the "Add Course" modal
	const openAdd = () => setShowAdd(true);
	const closeAdd = () => setShowAdd(false);

	/* 
	Function to open the "Edit Course" modal:
		- Fetches the selected course data using the course ID
		- Populates the values of the input fields in the modal form
		- Opens the "Edit Course" modal
	*/
	// const openEdit = (clientId) => {

	// 	// Fetches the selected course data using the course ID
		

	// 	fetch(`${ process.env.REACT_APP_API_URL }/clients/details`, {
	// 		method: 'GET',
	// 		headers: {
	// 			'Content-Type': 'application/json',
	// 			'Accept': 'application/json',	
	// 			Authorization: `Bearer ${ localStorage.getItem('token') }`
	// 		}
	// 		})
		
	// 	.then((res) => res.json())
	// 	.then((data) => {

	// 		console.log(data);

	// 		// Changes the states for binded to the input fields
	// 		// Populates the values of the input files in the modal form
	// 		setClientId(data._id);
	// 		setFullName(data.fullName);
	// 		setEmail(data.email);
	// 		setPassword(data.password);
	// 		setMobileNo(data.mobileNo);			
	// 	})
		

	// 	// Opens the "Edit Course" modal
	// 	setShowEdit(true);
	// };

	// /* 
	// Function to close our "Edit Course" modal:
	// 	- Reset from states back to their initial values
	// 	- Empties the input fields in the form whenever the modal is opened for adding a course
	// */
	// const closeEdit = () => {

	// 	setShowEdit(false);
	// 	setFullName("");
	// 	setEmail("");
	// 	setPassword("");
	// 	setMobileNo("");
		

	// };



	const newClient = (e) => {

		// Prevent the form from redirecting to a different page on submit 
		// Helps retain the data if adding a course is unsuccessful
		e.preventDefault()

		fetch(`${ process.env.REACT_APP_API_URL }/clients/register`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',		
				Authorization: `Bearer ${ localStorage.getItem('token') }`,

				 		'Access-Control-Allow-Origin':'*',
				 		'Access-Control-Allow-Methods':'POST,PATCH,OPTIONS'
			},
			body: JSON.stringify({

				fullName: fullName,				
				email: email,
				password: password,
				mobileNo: mobileNo
			})
		})
		.then(res => res.json())
		.then(data => {

			console.log(data)

			// If the new course is successfully added
			if (data === true) {

				// Invoke the "fetchData" function passed from our parent component (courses page)
				// Rerenders the page because of the "useEffect"
				fetchData();

				// Show a success message via sweet alert
				Swal.fire({
					title: "Success",
					icon: "success",
					text: "New client has been created."					
				})

				// Reset all states to their initial values
				// Provides better user experience by clearing all the input fieles when the user adds another course
				setFullName("");
				setEmail("");
				setPassword("");
				setMobileNo("");

				// Close the modal
				closeAdd();

			} else {

				fetchData();

				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again."
				})
			}
		})
	}

	const setAsAdmin = (clientId, isAdmin) => {
		
		//e.preventDefault();

		console.log(isAdmin);
		console.log(clientId);

		

		fetch(`${ process.env.REACT_APP_API_URL }/clients/setAsAdmin`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				'Accept': 'application/json',
				Authorization: `Bearer ${ localStorage.getItem('token') }`,
				'Access-Control-Allow-Origin':'*',
				 		'Access-Control-Allow-Methods':'POST,PATCH,OPTIONS'
			},
			body: JSON.stringify({
				clientId: !clientId,
				isAdmin: !isAdmin
			})
		})
		.then(res => res.json())
		.then(data => {

			if (data === true) {

				fetchData();

				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again."
				});

				//closeEdit();

			} else {

				fetchData();

				Swal.fire({
					
					title: "Success",
					icon: "success",
					text: "Client has been given administrative privileges."
				});

			}

		})
	}

	const revertAdmin = (clientId, isAdmin) => {
		
		//e.preventDefault();

		console.log(isAdmin);
		console.log(clientId);

		

		fetch(`${ process.env.REACT_APP_API_URL }/clients/removeAdmin`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				'Accept': 'application/json',
				Authorization: `Bearer ${ localStorage.getItem('token') }`,
				'Access-Control-Allow-Origin':'*',
				 		'Access-Control-Allow-Methods':'POST,PATCH,OPTIONS'
			},
			body: JSON.stringify({
				clientId: !clientId,
				isAdmin: !isAdmin
			})
		})
		.then(res => res.text())
		.then(data => {

			if (data === true) {

				fetchData();

				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again."
				});

				//closeEdit();

			} else {

				fetchData();

				Swal.fire({
					
					title: "Success",
					icon: "success",
					text: "Client has been given administrative privileges."
				});

			}

		})
	}

	// Map through the courses received from the parent component (course page)
	// Re-renders the table whenever the "coursesData" is updated by adding, editing and deleting a course
	useEffect(() => {

		const archiveToggle = (clientId, isActive) => {

					console.log(!isActive);

					fetch(`${ process.env.REACT_APP_API_URL }/clients/${ clientId }/archive`, {
						method: 'PUT',
						headers: {
							"Content-Type": "application/json",
							'Accept': 'application/json',
							Authorization: `Bearer ${ localStorage.getItem('token') }`,
							'Access-Control-Allow-Origin':'*',
				 		'Access-Control-Allow-Methods':'POST,PATCH,OPTIONS'
						},
						body: JSON.stringify({
							isActive: !isActive
						})
					})
					.then(res => res.json())
					.then(data => {

						if (data !== false) {

							fetchData();

							Swal.fire({
								title: "Success",
								icon: "success",
								text: "Client has been archived."
							});

						} else {

							fetchData();

							Swal.fire({
								title: "Something went wrong",
								icon: "error",
								text: "Please try again."
							});

						}
					})

			

				}

				const unArchiveToggle = (clientId, isActive) => {
						fetch(`${ process.env.REACT_APP_API_URL }/clients/${ clientId }/unArchived`, {
							method: "PUT",
							headers: {
								"Content-Type": "application/json",
								'Accept': 'application/json',
								"Authorization": `Bearer ${ localStorage.getItem('token') }`,
								'Access-Control-Allow-Origin':'*',
				 		'Access-Control-Allow-Methods':'POST,PATCH,OPTIONS'
							},
							body: JSON.stringify({
								isActive: !isActive
							})
						})
						.then(res => res.json())
						.then(data => {
							fetchData();
							if(data === true){
								Swal.fire({
									title: "Success",
									icon: "success",
									text: "Client is now active."
									}
								)
							} else {
								fetchData();
								Swal.fire({
									title: "Something went wrong",
									icon: "error",
									text: "Please try again."
								})
							}
						})
					}



		const clientsArr = clientsData.map(client => {
				
			return(


				
				<tr key={client._id}>
					<td >{client.fullName}</td>
					<td>{client.email}</td>					
					<td>{client.mobileNo}</td>
					<td>
						{/* 
							- If the course's "isActive" field is "true" displays "available"
							- Else if the course's "isActive" field is "false" displays "unavailable"
						*/}
						{client.isActive === true
							? <span>Active Client</span>
							: <span>Inactive</span>
						}
					</td>
					<td>{client.isAdmin
							? <span>Admin</span>
							: <span>Client</span>
						}
					</td>
					<td>

						{client.isAdmin === true ?
							<Button
								variant="muted"
								size="sm"
								onClick={() => revertAdmin(client._id, client.isAdmin)}
								className="m-1"
							>
								Remove Admin Access
							</Button>
							:
							<Button
								variant="muted"
								size="sm"
								onClick={() => setAsAdmin(client._id, client.isAdmin)}
								className="m-1"
							>
								Set as Admin
							</Button>
						}
						{/* 
							- Display a red "Disable" button if course is "active"
							- Else display a green "Enable" button if course is "inactive"
						*/}
						{client.isActive
							?
							<Button 
								variant="muted" 
								size="sm" 
								onClick={() => archiveToggle(client._id, client.isActive)}
								className="m-1"
							>
								Disable
							</Button>
							:
							<Button 
								variant="muted"
								size="sm"
								onClick={() => unArchiveToggle(client._id, client.isActive)}
								className="m-1"
							>
								Enable
							</Button>
						}
					</td>
				</tr>

			)

		});

		// Set the "courses" state with the table rows returned by the map function
		// Renders table row elements inside the table via this "AdminView" return statement below
		setClients(clientsArr);

	}, [clientsData, fetchData]);

	return(
		<Fragment>

			<div className="text-center my-4">
				<h2>Admin Dashboard</h2>
				<div className="d-flex justify-content-center">
					<Button variant="secondary" onClick={openAdd}>Add New Client</Button>			
				</div>			
			</div>

			<Table  hover responsive>
				<thead className="bg-secondary text-white">
					<tr>
						<th>Client Name</th>
						<th>Email</th>						
						<th>Mobile Number</th>
						<th>Status</th>
						<th>Access Type</th>
						<th>Actions</th>
					</tr>					
				</thead>
				<tbody>
					{clients}
				</tbody>
			</Table>

			{/*ADD MODAL*/}
			<Modal show={showAdd} onHide={closeAdd}>
				<Form onSubmit={e => newClient(e)}>
					<Modal.Header closeButton>
						<Modal.Title>Create New Client</Modal.Title>
					</Modal.Header>
					<Modal.Body>	
						<Form.Group controlId="fullName">
							<Form.Label>Client Name</Form.Label>
							<Form.Control type="text" value={fullName} onChange={e => setFullName(e.target.value)} required/>
						</Form.Group>
						<Form.Group controlId="email">
							<Form.Label>Email</Form.Label>
							<Form.Control type="text" value={email}  onChange={e => setEmail(e.target.value)} required/>
						</Form.Group>
						<Form.Group controlId="servicePrice">
							<Form.Label>Password</Form.Label>
							<Form.Control type="text" value={password}  onChange={e => setPassword(e.target.value)} required/>
						</Form.Group>
						<Form.Group controlId="mobileNo">
							<Form.Label>Mobile Number</Form.Label>
							<Form.Control type="string" value={mobileNo}  onChange={e => setMobileNo(e.target.value)} required/>
						</Form.Group>
					</Modal.Body>
					<Modal.Footer>
						<Button variant="secondary" onClick={closeAdd}>Close</Button>
						<Button variant="success" type="submit">Submit</Button>
					</Modal.Footer>
				</Form>
			</Modal>

			{/*EDIT MODAL*/}
			{/*<Modal >
				<Form onSubmit={e => setAsAdmin(e, clientId)}>
					<Modal.Header closeButton>
						<Modal.Title>Edit Client</Modal.Title>
					</Modal.Header>
					<Modal.Body>	
						<Form.Group controlId="fullName">
							<Form.Label>Client Name</Form.Label>
							<Form.Control type="text" value={fullName} onChange={e => setFullName(e.target.value)} required/>
						</Form.Group>
						<Form.Group controlId="serviceDescription">
							<Form.Label>Email</Form.Label>
							<Form.Control type="text" value={email}  onChange={e => setEmail(e.target.value)} required/>
						</Form.Group>
						<Form.Group controlId="servicePrice">
							<Form.Label>Password</Form.Label>
							<Form.Control type="number" value={password}  onChange={e => setPassword(e.target.value)} required/>
						</Form.Group>
						<Form.Group controlId="servicePrice">
							<Form.Label>Mobile Number</Form.Label>
							<Form.Control type="number" value={mobileNo}  onChange={e => setMobileNo(e.target.value)} required/>
						</Form.Group>
					</Modal.Body>
					<Modal.Footer>
						<Button variant="secondary">Close</Button>
						<Button variant="success" type="submit">Submit</Button>
					</Modal.Footer>
				</Form>
			</Modal>*/}
			
		</Fragment>
	)
}
