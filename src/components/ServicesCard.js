import {Card} from 'react-bootstrap';
//import { useState, useEffect } from 'react';
import {Link} from 'react-router-dom'


export default function ServicesCard({serviceProp}) {

    console.log(serviceProp);    
    console.log(typeof serviceProp);    

        const { serviceName, description, price, _id } = serviceProp
   
    return (
        <Card className = "cardHighlight m-5 p-5">
            <Card.Body>
                <Card.Header className="text-uppercase" as="h2">{serviceName}</Card.Header>
                

                <Card.Subtitle className="mt-4">Description:</Card.Subtitle>
                <Card.Text>{description}</Card.Text>

                <Card.Subtitle>Price:</Card.Subtitle>
                <Card.Text>{price}</Card.Text>


                <Link className="btn btn-secondary" to={`/services/${_id}`}>Details</Link>

            </Card.Body>
        </Card>
    )
}

